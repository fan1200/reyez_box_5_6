#!/usr/bin/env bash
apt-get purge apache2 -y;
apt-get install apache2 -y;
apt-get install libapache2-mod-php5.6 -y;
apt-get install php5.6 -y;
a2dismod worker;
a2enmod php5.6
a2dismod mpm_event
a2enmod mpm_prefork
a2enmod rewrite
apt-get install php5.6-mbstring
apt-get install php5.6-xml
sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php/5.6/apache2/php.ini
service apache2 restart
