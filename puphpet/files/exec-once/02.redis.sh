#!/usr/bin/env bash
cd /tmp;
wget https://github.com/phpredis/phpredis/archive/master.zip -O phpredis.zip;
unzip -o /tmp/phpredis.zip && mv /tmp/phpredis-* /tmp/phpredis && cd /tmp/phpredis && phpize && ./configure && make && sudo make install;
sudo touch /etc/php/5.6/mods-available/redis.ini && echo extension=redis.so > /etc/php/5.6/mods-available/redis.ini;
sudo ln -s /etc/php/5.6/mods-available/redis.ini /etc/php/5.6/apache2/conf.d/;
sudo ln -s /etc/php/5.6/mods-available/redis.ini /etc/php/5.6/fpm/conf.d/;
sudo ln -s /etc/php/5.6/mods-available/redis.ini /etc/php/5.6/cli/conf.d/;